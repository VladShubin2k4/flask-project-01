FROM python:3.8-slim

COPY . /app
WORKDIR /app

RUN pip install --no-cache-dir -r requirements.txt
RUN pip install -r requirements-server.txt

RUN flask db upgrade
RUN python seed.py

EXPOSE 8000
CMD [ "gunicorn", "app:app", "-b", "0.0.0.0:8000" ]

